package com.agero.nccsdk;

import com.agero.nccsdk.domain.data.NccSensorData;

/**
 * Interface definition for a callback to be invoked on sensor data changes.
 */
public interface NccSensorListener {

    /**
     * Invoked when sensor data is available
     *
     * @param data NccSensorData containing the available sensor data
     */
    void onDataChanged(NccSensorType sensorType, NccSensorData data);

}
