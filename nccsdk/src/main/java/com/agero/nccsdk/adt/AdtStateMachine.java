package com.agero.nccsdk.adt;

import com.agero.nccsdk.adt.detection.AdtTriggerReceiver;
import com.agero.nccsdk.internal.common.statemachine.AbstractStateMachine;
import com.agero.nccsdk.internal.log.Timber;

public class AdtStateMachine extends AbstractStateMachine<AdtState> implements AdtTriggerReceiver.OnTriggerReceived {
    public static final String ACTION_TRIGGER_UPDATE = "ACTION_TRIGGER_UPDATE";
    public static final String TRIGGER_TYPE = "TRIGGER_TYPE";

    public AdtStateMachine(AdtState currentState) {
        super(currentState);
    }

    @Override
    public void onTriggerReceived(AdtTriggerType adtTriggerType) {
        handleTrigger(adtTriggerType);
    }

    private void handleTrigger(AdtTriggerType triggerType) {
        Timber.d("Received trigger " + triggerType.getName());
        switch (triggerType) {
            case START_MONITORING:
                this.currentState.startMonitoring(this);
                break;
            case START_MOTION:
            case START_WIFI:
            case START_GEOFENCE:
            case START_SERVER:
                this.currentState.startDriving(this);
                break;
            case STOP_MOTION:
            case STOP_LOCATION_TIMER:
            case STOP_LOCATION_MODE:
            case STOP_SERVER:
                this.currentState.stopDriving(this);
                break;
            default:
                Timber.e("Unknown trigger type: " + triggerType.getName());
        }
    }

    public boolean isDriveInProgress() {
        return currentState instanceof AdtDrivingState;
    }
}
