package com.agero.nccsdk.adt.detection.start.algo;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.agero.nccsdk.adt.AdtStateMachine;
import com.agero.nccsdk.adt.AdtTriggerType;
import com.agero.nccsdk.adt.detection.EvaluateStrategy;
import com.agero.nccsdk.adt.detection.ReleasableResources;
import com.agero.nccsdk.domain.data.NccSensorData;
import com.agero.nccsdk.internal.common.util.DeviceUtils;
import com.agero.nccsdk.internal.log.Timber;

/**
 * Created by james hermida on 11/30/17.
 */

public abstract class DrivingStartStrategy<T extends NccSensorData> implements EvaluateStrategy<T>, ReleasableResources {

    final Context context;

    DrivingStartStrategy(Context context) {
        this.context = context;
    }

    void sendCollectionStartBroadcast(AdtTriggerType startTrigger) {
        if (DeviceUtils.isLocationSettingOnWithHighestAccuracy(context)) {
            Intent intent = new Intent(AdtStateMachine.ACTION_TRIGGER_UPDATE);
            intent.putExtra(AdtStateMachine.TRIGGER_TYPE, startTrigger);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
        } else {
            Timber.w("Attempted to start collection but the location setting is off or is not set to high accuracy");
        }
    }

}
