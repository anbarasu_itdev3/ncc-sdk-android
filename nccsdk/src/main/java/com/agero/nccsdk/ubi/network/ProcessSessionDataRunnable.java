package com.agero.nccsdk.ubi.network;

import android.content.Context;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccExceptionErrorCode;
import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.internal.common.io.FileCompressor;
import com.agero.nccsdk.internal.common.util.DeviceUtils;
import com.agero.nccsdk.internal.common.util.FileUtils;
import com.agero.nccsdk.internal.di.qualifier.UbiDataPath;
import com.agero.nccsdk.internal.log.Timber;

import org.apache.commons.compress.archivers.ArchiveException;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

/**
 * Created by james hermida on 10/31/17.
 */

public class ProcessSessionDataRunnable implements Runnable {

    private final String TAG = ProcessSessionDataRunnable.class.getSimpleName();

    @Inject
    Context context;

    @Inject
    UbiSyncManager dataManager;

    @Inject
    FileCompressor fileCompressor;

    @Inject
    @UbiDataPath
    File postDataPath;

    private final String sessionId;
    private final File sessionFilesPathFile;

    public ProcessSessionDataRunnable(File sessionFilesPathFile) {
        NccSdk.getComponent().inject(this);

        this.sessionId = sessionFilesPathFile.getName();
        this.sessionFilesPathFile = sessionFilesPathFile;
    }

    @Override
    public void run() {
        if (FileUtils.isNullOrDoesNotExist(sessionFilesPathFile)) {
            Timber.e("Session files path is null or does not exist: %s", sessionFilesPathFile);
        } else {
            Timber.d("Starting data compression for session id: %s", sessionId);
            try {
                deleteExistingGzipFiles();
                gzipRawFiles();
                deleteRawFiles();
                uploadData(tarGzipFiles());
                deleteSessionFilesPath();
            } catch (NccException | ArchiveException | IOException e) {
                Timber.e(e, "Exception caught processing data for session id: %s", sessionId);
            }
        }
    }

    private void deleteExistingGzipFiles() throws NccException {
        File[] files = sessionFilesPathFile.listFiles();
        if (FileUtils.isNullOrEmpty(files)) {
            throwNccException("Provided files is null or empty");
        } else {
            for (File file : files) {
                if (file.getName().endsWith(FileCompressor.EXTENSION_GZIP)) {
                    if (file.delete()) {
                        Timber.v("Existing gzip file deleted: %s", file.getAbsolutePath());
                    } else {
                        throwNccException("Failed to delete gzip file");
                    }
                }
            }
        }
    }

    private void deleteRawFiles() throws NccException {
        File[] files = sessionFilesPathFile.listFiles();
        if (FileUtils.isNullOrEmpty(files)) {
            throwNccException("Provided files is null or empty");
        } else {
            for (File file : files) {
                if (!file.getName().endsWith(FileCompressor.EXTENSION_GZIP)) {
                    if (file.delete()) {
                        Timber.v("Raw data file deleted: %s", file.getAbsolutePath());
                    } else {
                        throwNccException("Failed to delete raw data file");
                    }
                }
            }
        }
    }

    private void gzipRawFiles() throws NccException {
        File[] files = sessionFilesPathFile.listFiles();
        if (FileUtils.isNullOrEmpty(files)) {
            throwNccException("Cannot gzip data. Provided files is null or empty");
        } else {
            String gzipOutputDir = sessionFilesPathFile.toString();
            for (File rawFile : files) {
                File gzip = fileCompressor.gzip(rawFile, gzipOutputDir);
                if (FileUtils.isNullOrDoesNotExist(gzip)) {
                    throwNccException("Failed to gzip file. Result file is null or does not exist: " + gzip);
                } else {
                    Timber.v("Gzip file created: %s", gzip.getAbsolutePath());
                }
            }
        }
    }

    private File tarGzipFiles() throws IOException, ArchiveException, NccException {
        String outputTarAbsPath = postDataPath + File.separator + sessionId;
        File tar = fileCompressor.tar(sessionFilesPathFile, outputTarAbsPath);
        if (FileUtils.isNullOrDoesNotExist(tar)) {
            throwNccException("Exception creating tar archive. Result tar is null or does not exist:" + tar);
        }
        return tar;
    }

    private void uploadData(File file) throws NccException {
        if (FileUtils.isNullOrDoesNotExist(file)) {
            throwNccException("Cannot perform upload. File is null or does not exist: " + file);
        } else {
            if (DeviceUtils.isConnectedToWiFi(context)) {
                Timber.d("Starting upload for session id: %s", sessionId);
                dataManager.uploadSession(file);
            } else {
                Timber.d("Skipping upload due to no wifi connection for session id: %s", sessionId);
            }
        }
    }

    private void deleteSessionFilesPath() throws NccException {
        // Delete files
        File[] files = sessionFilesPathFile.listFiles();
        if (files != null && files.length > 0) {
            for (File file : files) {
                if (file.delete()) {
                    Timber.v("File deleted: %s", file.getAbsolutePath());
                } else {
                    throwNccException("Failed to delete file: " + file.getAbsolutePath());
                }
            }
        }
        
        if (sessionFilesPathFile.listFiles().length == 0) {
            if (sessionFilesPathFile.delete()) {
                Timber.v("Session data folder deleted: %s", sessionFilesPathFile.getAbsolutePath());
            } else {
                throwNccException("Failed to delete session data folder: " + sessionFilesPathFile.getAbsolutePath());
            }
        }
    }

    private void throwNccException(String msg) throws NccException {
        throw new NccException(TAG, msg, NccExceptionErrorCode.UNKNOWN_ERROR);
    }
}
