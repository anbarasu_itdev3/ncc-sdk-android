package com.agero.nccsdk.ubi.network;

import java.io.File;

public interface UbiSyncManager {

    void uploadSession(File file);
    void uploadAllSessions();

}
