package com.agero.nccsdk.domain.sensor;

import android.content.Context;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccExceptionErrorCode;
import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.domain.data.NccSensorData;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by james hermida on 8/16/17.
 *
 * Abstract class to serve as a base class for all sensors. This class manages all the subscribers and sends data to
 * the subscribers of this sensor.
 */

public abstract class NccAbstractSensor implements NccSensor {

    private final String TAG = NccAbstractSensor.class.getSimpleName();

    protected final Context applicationContext;
    boolean isStreaming = false;
    protected NccConfig config;
    final NccSensorType sensorType;

    private CopyOnWriteArrayList<NccSensorListener> listeners;

    /**
     * Creates an instance of NccAbstractSensor. Invoked by a subclass's constructor.
     * @param context The Context allowing access to application-specific resources and classes, as well as
     * up-calls for application-level operations such as launching activities,
     * broadcasting and receiving intents, etc.
     */
    protected NccAbstractSensor(Context context, NccSensorType sensorType, NccConfig config) {
        this.applicationContext = context.getApplicationContext();
        this.sensorType = sensorType;
        this.config = config;
    }

    /**
     * See {@link NccSensor#isStreaming()}
     */
    public boolean isStreaming() {
        return this.isStreaming;
    }

    /**
     * See {@link NccSensor#hasSubscribers()}
     */
    @Override
    public boolean hasSubscribers() { // TODO remove if needed
        return listeners != null && listeners.size() > 0;
    }

    @Override
    public NccConfig getConfig() {
        return config;
    }

    @Override
    public void setConfig(NccConfig config) {
        this.config = config;
        onConfigChanged(config);
    }

    @Override
    public void onConfigChanged(NccConfig config) {
        if (isStreaming) {
            stopStreaming();
            startStreaming();
        }
    }

    /**
     * See {@link NccSensor#subscribe(NccSensorListener)}
     */
    @Override
    public void subscribe(NccSensorListener listener) throws NccException {
        if (listener != null) {
            if (listeners == null) {
                listeners = new CopyOnWriteArrayList<>();
            }

            if (listeners.contains(listener)) {
                throw new NccException(TAG, "NccSensorListener already subscribed", NccExceptionErrorCode.UNKNOWN_ERROR);
            }

            listeners.add(listener);
        }
    }

    /**
     * See {@link NccSensor#unsubscribe(NccSensorListener)}
     */
    @Override
    public void unsubscribe(NccSensorListener listener) {
        if (listener != null) {
            if (listeners != null) {
                listeners.remove(listener);
            }
        }
    }

    /**
     * Posts data to this sensor's subscribers
     *
     * @param data Data to be sent to the subscribers
     */
    void postData(NccSensorData data) {
        for (NccSensorListener listener : listeners) {
            listener.onDataChanged(sensorType, data);
        }
    }
}
