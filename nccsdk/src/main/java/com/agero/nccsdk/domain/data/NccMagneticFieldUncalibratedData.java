package com.agero.nccsdk.domain.data;

import java.util.Locale;

/**
 * Created by james hermida on 8/24/17.
 */

public class NccMagneticFieldUncalibratedData extends NccAbstractSensorData {

    private final float biasX;
    private final float biasY;
    private final float biasZ;
    private final float ironX;
    private final float ironY;
    private final float ironZ;
    private final int accuracy;

    /**
     * Creates an instance of NccMagneticFieldData
     *
     * @param timestamp UTC timestamp in milliseconds
     * @param biasX The earth’s magnetic field plus surrounding fields, minus device bias in the x direction
     * @param biasY The earth’s magnetic field plus surrounding fields, minus device bias in the y direction
     * @param biasZ The earth’s magnetic field plus surrounding fields, minus device bias in the z direction
     * @param ironX The iron bias estimation along the x axis
     * @param ironY The iron bias estimation along the y axis
     * @param ironZ The iron bias estimation along the z axis
     * @param accuracy The accuracy of this data where -1 is uncalibrated, 0 is low accuracy, 1 is medium accuracy and 2 is high accuracy
     */
    public NccMagneticFieldUncalibratedData(long timestamp, float biasX, float biasY, float biasZ, float ironX, float ironY, float ironZ, int accuracy) {
        super(timestamp);
        this.biasX = biasX;
        this.biasY = biasY;
        this.biasZ = biasZ;
        this.ironX = ironX;
        this.ironY = ironY;
        this.ironZ = ironZ;
        this.accuracy = accuracy;
    }

    /**
     * Gets the earth’s magnetic field plus surrounding fields, minus device bias in the x direction
     *
     * @return float of the earth’s magnetic field plus surrounding fields, minus device bias in the x direction
     */
    public float getBiasX() {
        return biasX;
    }

    /**
     * Gets the earth’s magnetic field plus surrounding fields, minus device bias in the y direction
     *
     * @return float of the earth’s magnetic field plus surrounding fields, minus device bias in the y direction
     */
    public float getBiasY() {
        return biasY;
    }

    /**
     * Gets the earth’s magnetic field plus surrounding fields, minus device bias in the z direction
     *
     * @return float of the earth’s magnetic field plus surrounding fields, minus device bias in the z direction
     */
    public float getBiasZ() {
        return biasZ;
    }

    /**
     * Gets the iron bias estimation along the x axis
     *
     * @return float of the iron bias estimation along the x axis
     */
    public float getIronX() {
        return ironX;
    }

    /**
     * Gets the iron bias estimation along the y axis
     *
     * @return float of the iron bias estimation along the y axis
     */
    public float getIronY() {
        return ironY;
    }

    /**
     * Gets the iron bias estimation along the z axis
     *
     * @return float of the iron bias estimation along the z axis
     */
    public float getIronZ() {
        return ironZ;
    }

    /**
     * Gets the accuracy of this data where -1 is uncalibrated, 0 is low accuracy, 1 is medium accuracy and 2 is high accuracy
     *
     * @return int of the accuracy
     */
    public int getAccuracy() {
        return accuracy;
    }

    @Override
    public String toString() {
        return "NccMagneticFieldUncalibratedData{" +
                "biasX=" + biasX +
                ", biasY=" + biasY +
                ", biasZ=" + biasZ +
                ", ironX=" + ironX +
                ", ironY=" + ironY +
                ", ironZ=" + ironZ +
                ", accuracy=" + accuracy +
                ", timestamp=" + timestamp +
                '}';
    }
}
