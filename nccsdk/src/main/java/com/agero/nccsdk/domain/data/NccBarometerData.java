package com.agero.nccsdk.domain.data;

import java.util.Locale;

/**
 * Created by james hermida on 8/25/17.
 */

public class NccBarometerData extends NccAbstractSensorData {

    private final float pressure;

    /**
     * Creates an instance of NccBarometerData
     *
     * @param timestamp UTC timestamp in milliseconds
     * @param pressure Atmospheric pressure in hPa (millibar)
     */
    public NccBarometerData(long timestamp, float pressure) {
        super(timestamp);
        this.pressure = pressure;
    }

    /**
     * Gets the atmospheric pressure in hPa (millibar)
     *
     * @return float of the atmospheric pressure in hPa (millibar)
     */
    public float getPressure() {
        return pressure;
    }

    @Override
    public String toString() {
        return "NccBarometerData{" +
                "pressure=" + pressure +
                ", timestamp=" + timestamp +
                '}';
    }
}
