package com.agero.nccsdk.domain.sensor;

import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;

import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.domain.data.NccBluetoothData;
import com.agero.nccsdk.domain.model.NccBluetoothClass;
import com.agero.nccsdk.domain.model.NccBluetoothDevice;
import com.agero.nccsdk.internal.common.util.StringUtils;
import com.agero.nccsdk.internal.log.Timber;

@SuppressWarnings({"MissingPermission"})
public class NccBluetooth extends NccAbstractBroadcastReceiverSensor {

    private final int PROFILE_HEADSET = 0;
    private final int PROFILE_A2DP = 1;

    public NccBluetooth(Context context, NccConfig config) {
        super(context, NccBluetooth.class.getSimpleName(), NccSensorType.BLUETOOTH, config);
    }

    @Override
    String[] getActions() {
        return new String[]{
                "android.bluetooth.device.action.ACL_CONNECTED",
                "android.bluetooth.device.action.ACL_DISCONNECTED",
                "android.bluetooth.device.action.ACL_DISCONNECT_REQUESTED"
        };
    }

    @Override
    boolean isLocalReceiver() {
        return false;
    }

    @Override
    public void onDataReceived(Context context, Intent intent) {
        String action = intent.getAction();
        if (StringUtils.isNullOrEmpty(action)) {
            Timber.e("Bluetooth broadcast action is null or empty");
        } else {
            BluetoothDevice bluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            if (bluetoothDevice == null) {
                Timber.e("Bluetooth device extra missing from intent");
            } else {
                postData(new NccBluetoothData(
                        System.currentTimeMillis(),
                        getDevice(bluetoothDevice, action.equals(BluetoothDevice.ACTION_ACL_CONNECTED))
                ));
            }
        }
    }

    private NccBluetoothDevice getDevice(BluetoothDevice bluetoothDevice, boolean isDeviceConnected) {
        NccBluetoothClass bluetoothClass = new NccBluetoothClass(
                doesClassMatch(PROFILE_A2DP, bluetoothDevice.getBluetoothClass()),
                doesClassMatch(PROFILE_HEADSET, bluetoothDevice.getBluetoothClass())
        );

        return new NccBluetoothDevice(
                bluetoothDevice.getName(),
                bluetoothDevice.getAddress(),
                isDeviceConnected,
                bluetoothClass
        );
    }

    /**
     * Check class bits for possible bluetooth profile support.
     * This is a simple heuristic that tries to guess if a device with the
     * given class bits might support specified profile. It is not accurate for all
     * devices. It tries to err on the side of false positives.
     *
     * @param profile        The profile to be checked
     * @param bluetoothClass The bluetooth class to be checked
     * @return True if this device might support specified profile.
     */
    private boolean doesClassMatch(int profile, BluetoothClass bluetoothClass) {
        if (profile == PROFILE_A2DP) {
            switch (bluetoothClass.getDeviceClass()) {
                case BluetoothClass.Device.AUDIO_VIDEO_HIFI_AUDIO:
                case BluetoothClass.Device.AUDIO_VIDEO_HEADPHONES:
                case BluetoothClass.Device.AUDIO_VIDEO_LOUDSPEAKER:
                case BluetoothClass.Device.AUDIO_VIDEO_CAR_AUDIO:
                    return true;
                default:
                    return false;
            }
        } else if (profile == PROFILE_HEADSET) {
            switch (bluetoothClass.getDeviceClass()) {
                case BluetoothClass.Device.AUDIO_VIDEO_HANDSFREE:
                case BluetoothClass.Device.AUDIO_VIDEO_WEARABLE_HEADSET:
                case BluetoothClass.Device.AUDIO_VIDEO_CAR_AUDIO:
                    return true;
                default:
                    return false;
            }
        }
        return false;
    }
}
