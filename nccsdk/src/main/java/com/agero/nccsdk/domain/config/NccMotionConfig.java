package com.agero.nccsdk.domain.config;

/**
 * Created by james hermida on 8/30/17.
 */

public class NccMotionConfig extends NccAbstractConfig {

    private int interval = 5000; // Default 5 seconds

    public NccMotionConfig() {
        super();
    }

    public NccMotionConfig(int interval) {
        this.interval = interval;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }
}
