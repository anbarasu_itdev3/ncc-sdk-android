package com.agero.nccsdk.lbt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.agero.nccsdk.internal.log.Timber;

/**
 * Created by james hermida on 11/8/17.
 */

public class LbtActionReceiver extends BroadcastReceiver {

    private final Callback callback;

    public LbtActionReceiver(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.hasExtra(LbtCollectionState.ACTION_LBT)) {
            LbtAction action = (LbtAction) intent.getSerializableExtra(LbtCollectionState.ACTION_LBT);
            notifyCallback(action);
        } else {
            Timber.w("Broadcast received with no ACTION_LBT extra");
        }
    }

    public interface Callback {
        void onStart();
        void onStop();
    }

    private void notifyCallback(LbtAction action) {
        if (callback == null) {
            Timber.w("%s Callback is null", LbtActionReceiver.class.getSimpleName());
        } else {
            switch (action) {
                case START:
                    callback.onStart();
                    break;
                case STOP:
                    callback.onStop();
                    break;
                default:
                    Timber.w("Unhandled action received: %s", action.getName());
            }
        }
    }
}
