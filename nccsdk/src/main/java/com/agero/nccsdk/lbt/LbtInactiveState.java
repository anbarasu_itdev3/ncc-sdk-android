package com.agero.nccsdk.lbt;

import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.internal.common.statemachine.StateMachine;
import com.agero.nccsdk.internal.log.Timber;

import java.util.List;

/**
 * Created by james hermida on 11/6/17.
 */

public class LbtInactiveState extends LbtAbstractState {

    public LbtInactiveState() {
        super();
    }

    public LbtInactiveState(List<NccSensorListener> listeners) {
        this.trackingListeners = listeners;
    }

    @Override
    public void onEnter() {
        Timber.d("Entered LbtInactiveState");
    }

    @Override
    public void onExit() {
        Timber.d("Exited LbtInactiveState");
    }

    @Override
    public void onDrivingStart(StateMachine<LbtState> machine) {
        machine.setState(new LbtCollectionState(trackingListeners));
    }

    @Override
    public void onDrivingStop(StateMachine<LbtState> machine) {
        // DO NOTHING
    }
}
