package com.agero.nccsdk.lbt.util;

import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;

import com.agero.nccsdk.internal.data.network.aws.kinesis.KinesisClient;
import com.agero.nccsdk.internal.di.qualifier.LbtKinesisClient;

import javax.inject.Inject;

import com.agero.nccsdk.internal.log.Timber;

/**
 * Created by james hermida on 11/8/17.
 */

public class KinesisUploadTimedOperation implements TimedOperation {
    
    private HandlerThread handlerThread;
    private Handler handler;
    private final KinesisClient kinesisClient;
    
    private long timeInterval;    
    private final Runnable uploadRunnable = new Runnable() {
        @Override
        public void run() {
            kinesisClient.submit();
            scheduleOperation(timeInterval);
        }
    };
    
    @Inject
    public KinesisUploadTimedOperation(@LbtKinesisClient KinesisClient kinesisClient) {
        this.kinesisClient = kinesisClient;
    }

    @Override
    public void start(long timeInterval) {
        if (isRunning()) {
            Timber.v("Timer is already running");
        } else {
            this.timeInterval = timeInterval;
            startHandlers();
            scheduleOperation(timeInterval);
        }
    }

    @Override
    public void stop() {
        stopHandlers();
    }

    @Override
    public boolean isRunning() {
        return handlerThread != null && handlerThread.isAlive();
    }

    private void startHandlers() {
        handlerThread = new HandlerThread(KinesisUploadTimedOperation.class.getSimpleName() + HandlerThread.class.getSimpleName());
        handlerThread.start();
        handler = new Handler(handlerThread.getLooper());
    }
    
    private void stopHandlers() {
        if (isRunning()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                Timber.v("Handler thread quit safely result: %s", handlerThread.quitSafely());
            } else {
                Timber.v("Handler thread quit result: %s", handlerThread.quit());
            }
        }
    }
    
    private void scheduleOperation(long timeInterval) {
        handler.postDelayed(uploadRunnable, timeInterval);
    }

    public long getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(long timeInterval) {
        this.timeInterval = timeInterval;
    }
}
