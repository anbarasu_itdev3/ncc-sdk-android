package com.agero.nccsdk;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.PowerManager;
import android.support.annotation.DrawableRes;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.agero.nccsdk.adt.AdtState;
import com.agero.nccsdk.adt.AdtStateMachine;
import com.agero.nccsdk.adt.AdtTriggerType;
import com.agero.nccsdk.adt.detection.AdtTriggerReceiver;
import com.agero.nccsdk.adt.event.AdtEventHandler;
import com.agero.nccsdk.adt.event.AdtEventListener;
import com.agero.nccsdk.domain.SensorManager;
import com.agero.nccsdk.domain.config.NccWifiConfig;
import com.agero.nccsdk.domain.data.NccWifiData;
import com.agero.nccsdk.domain.sensor.ForegroundService;
import com.agero.nccsdk.internal.ForegroundManager;
import com.agero.nccsdk.internal.auth.AuthManager;
import com.agero.nccsdk.internal.common.statemachine.SdkConfigType;
import com.agero.nccsdk.internal.common.statemachine.StateMachine;
import com.agero.nccsdk.internal.common.util.AppUtils;
import com.agero.nccsdk.internal.common.util.Clock;
import com.agero.nccsdk.internal.common.util.StringUtils;
import com.agero.nccsdk.internal.data.DataManager;
import com.agero.nccsdk.internal.data.network.aws.kinesis.KinesisClient;
import com.agero.nccsdk.internal.di.DaggerSdkComponent;
import com.agero.nccsdk.internal.di.SdkComponent;
import com.agero.nccsdk.internal.di.module.SdkModule;
import com.agero.nccsdk.internal.di.qualifier.LogKinesisClient;
import com.agero.nccsdk.internal.log.DebugTree;
import com.agero.nccsdk.internal.log.ReleaseTree;
import com.agero.nccsdk.internal.log.Timber;
import com.agero.nccsdk.internal.log.config.LogConfig;
import com.agero.nccsdk.internal.notification.NotificationManager;
import com.agero.nccsdk.lbt.LbtActionReceiver;
import com.agero.nccsdk.lbt.LbtState;
import com.agero.nccsdk.lbt.LbtStateMachine;
import com.agero.nccsdk.lbt.network.LbtDataManager;
import com.agero.nccsdk.lbt.network.LbtSyncManager;
import com.agero.nccsdk.lbt.util.KinesisUploadTimedOperation;
import com.agero.nccsdk.lbt.util.TimedOperation;
import com.agero.nccsdk.ubi.UbiState;
import com.agero.nccsdk.ubi.UbiStateMachine;
import com.agero.nccsdk.ubi.network.UbiSyncManager;

import java.security.InvalidParameterException;

import javax.inject.Inject;
import javax.inject.Provider;

import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.POWER_SERVICE;
import static com.agero.nccsdk.adt.AdtStateMachine.ACTION_TRIGGER_UPDATE;
import static com.agero.nccsdk.adt.event.AdtEventHandler.ACTION_SESSION_END;
import static com.agero.nccsdk.adt.event.AdtEventHandler.ACTION_SESSION_START;
import static com.agero.nccsdk.lbt.LbtCollectionState.ACTION_LBT;

/**
 * Created by james hermida on 8/10/17.
 *
 * Entry-point into the NCC SDK.
 */

public class NccSdk implements NccSdkInterface {

    private static NccSdk nccSdk;
    private static Context applicationContext;
    private static SdkComponent sdkComponent;
    private static volatile boolean isInitialized = false;

    private final Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler;

    @Inject
    AuthManager authManager;

    @Inject
    NotificationManager notificationManager;

    @Inject
    StateMachine<LbtState> lbtStateMachine;

    @Inject
    StateMachine<UbiState> ubiStateMachine;

    @Inject
    StateMachine<AdtState> adtStateMachine;

    @Inject
    AdtEventHandler adtEventHandler;

    @Inject
    SensorManager sensorManager;

    @Inject
    DataManager dataManager;

    @Inject
    Clock clock;

    @Inject
    Provider<LbtSyncManager> lbtSyncManagerProvider;

    @Inject
    Provider<UbiSyncManager> ubiSyncManagerProvider;

    @Inject
    @LogKinesisClient
    Provider<KinesisClient> logKinesisClientProvider;

    private Timber.Tree timberTree;

    private NccSensorListener uploadTripsWifiListener = (sensorType, data) -> {
        if (((NccWifiData) data).isConnected()) {
            UbiSyncManager ubiSyncManager = ubiSyncManagerProvider.get();
            ubiSyncManager.uploadAllSessions();
        }
    };

    private AdtEventListener uploadTripsAdtListener = new AdtEventListener() {
        @Override
        public void onDrivingStart() {

        }

        @Override
        public void onDrivingStop() {
            UbiSyncManager ubiSyncManager = ubiSyncManagerProvider.get();
            ubiSyncManager.uploadAllSessions();
        }
    };

    private IgnoringBatteryOptimizationsCallback ignoringBatteryOptimizationsCallback;

    public static NccSdkInterface getNccSdk(final Application application) {
        if (application == null) {
            throw new InvalidParameterException("Context cannot be null");
        }

        if (nccSdk == null) {
            nccSdk = new NccSdk(application);
        }

        return nccSdk;
    }

    private NccSdk(final Application application) {
        applicationContext = application.getApplicationContext();

        ForegroundManager.init(application);
        ForegroundManager.get().addListener(new ForegroundManager.Listener() {
            @Override
            public void onAppForeground() {
                PowerManager powerManager = (PowerManager) applicationContext.getSystemService(POWER_SERVICE);
                if (ignoringBatteryOptimizationsCallback != null
                        && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                        && (powerManager != null && !powerManager.isIgnoringBatteryOptimizations(AppUtils.getAppPackage(applicationContext)))) {
                    ignoringBatteryOptimizationsCallback.onDisabled();
                }
            }

            @Override
            public void onAppBackground() {

            }
        });

        initDaggerComponent();
        sdkComponent.inject(this);

        // Maintain a reference to the default UncaughtExceptionHandler to pass exception
        // to it from after the custom exception handler has processed it
        defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();

        // Setup handler for uncaught exceptions.
        Thread.setDefaultUncaughtExceptionHandler((thread, exception) -> {
            if (timberTree != null) {
                if (exception == null) {
                    Timber.e("Uncaught exception occurred but exception is null");
                } else {
                    Timber.e(exception, "Uncaught exception occurred: %s", exception.getMessage());
                }
            }
            defaultUncaughtExceptionHandler.uncaughtException(thread, exception);
        });

        initAdt();
        registerAdtEventListeners();
        startTrackingIfNeeded();
    }

    private static void initDaggerComponent() {
        sdkComponent = DaggerSdkComponent
                .builder()
                .sdkModule(new SdkModule(applicationContext))
                .build();
    }

    public static SdkComponent getComponent() {
        if (sdkComponent == null) {
            initDaggerComponent();
        }
        return sdkComponent;
    }

    public static Context getApplicationContext() {
        return applicationContext;
    }

    /**
     * Returns the current version of the SDK as a string.
     *
     * @return the current version of the SDK
     */
    public static String getSdkVersion() {
        return NccSdkVersion.BUILD;
    }

    @Override
    public void authenticate(final String apiKey, final AuthenticateCallback authenticateCallback) {
        if (isInitialized) {
            Timber.d("SDK already initialized");
        } else if (StringUtils.isNullOrEmpty(apiKey)) {
            Log.e(NccSdk.class.getSimpleName(), "apiKey cannot be null or empty");
        } else {
            authManager.authenticate(apiKey)
                    .subscribeOn(Schedulers.io())
                    .subscribe(new DisposableCompletableObserver() {
                        @Override
                        public void onComplete() {
                            Log.d(NccSdk.class.getSimpleName(), "Successfully authenticated SDK with api key: " + apiKey);
                            initialize();
                            if (authenticateCallback != null) {
                                authenticateCallback.onSuccess();
                            }
                        }

                        @Override
                        public void onError(Throwable t) {
                            initialize();
                            if (authenticateCallback != null) {
                                authenticateCallback.onFailure(t);
                            }
                        }
                    });
        }
    }

    private synchronized void initialize() {
        if (!isInitialized) { // FIXME there may be a better solution to restrict execution of init code only once
            isInitialized = true;
            initLogger();
            initLbtReceiver();
            registerWifiListener();
            Timber.d("SDK initialized");
        }
    }

    private void initLogger() {
        // Get log config
        LogConfig logConfig;
        NccException configException = null;
        try {
            logConfig = (LogConfig) dataManager.getSdkConfig(SdkConfigType.LOG);
        } catch (NccException e) {
            logConfig = new LogConfig();
            configException = e; // keep track of exception so it can be logged after Timber is initialized
        }

        // Initialize Timber
        KinesisClient logKinesisClient = logKinesisClientProvider.get();
        if (BuildConfig.DEBUG) {
            timberTree = new DebugTree(logConfig, dataManager, logKinesisClient, clock);
        } else {
            timberTree = new ReleaseTree(logConfig, dataManager, logKinesisClient, clock);
        }
        Timber.plant(timberTree);

        // Log exception if needed
        if (configException != null) {
            Timber.e(configException, "Exception getting log config. Used default config.");
        }

        // Start logging timer
        TimedOperation uploadTimedOperation = new KinesisUploadTimedOperation(logKinesisClient);
        uploadTimedOperation.start(logConfig.getUploadInterval());
    }

    private void initAdt() {
        // Register ADT receiver
        AdtTriggerReceiver adtTriggerReceiver = new AdtTriggerReceiver((AdtStateMachine) adtStateMachine);
        LocalBroadcastManager.getInstance(applicationContext).registerReceiver(adtTriggerReceiver, new IntentFilter(ACTION_TRIGGER_UPDATE));

        // Send ADT start detection broadcast
        Intent i = new Intent(ACTION_TRIGGER_UPDATE);
        i.putExtra(AdtStateMachine.TRIGGER_TYPE, AdtTriggerType.START_MONITORING);
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(i);
    }

    private void registerAdtEventListeners() {
        IntentFilter intentFilter = new IntentFilter(ACTION_SESSION_START);
        intentFilter.addAction(ACTION_SESSION_END);
        LocalBroadcastManager.getInstance(applicationContext).registerReceiver(adtEventHandler, intentFilter);

        adtEventHandler.registerListener((UbiStateMachine) ubiStateMachine);
        adtEventHandler.registerListener(uploadTripsAdtListener);
    }

    private void initLbtReceiver() {
        LbtSyncManager lbtSyncManager = lbtSyncManagerProvider.get();

        // Register LBT receiver for the transfer manager FIXME this was put here due to failing tests with LocalBroadcastManager.registerReceiver()
        LbtActionReceiver lbtActionReceiver = new LbtActionReceiver((LbtDataManager) lbtSyncManager);
        LocalBroadcastManager.getInstance(applicationContext).registerReceiver(lbtActionReceiver, new IntentFilter(ACTION_LBT));
    }

    private void registerWifiListener() {
        try {
            sensorManager.subscribeSensorListener(NccSensorType.WIFI, uploadTripsWifiListener, new NccWifiConfig());
        } catch (NccException ne) {
            Timber.w("Failed to register wifi listener");
        }
    }

    private void startTrackingIfNeeded() {
        if (dataManager.shouldLbtAutoRestart()) {
            Timber.d("Auto restarting LBT");
            startTracking();
        }
    }

    @Override
    public void setUserId(String userId) {
        dataManager.setUserId(userId);
    }

    @Override
    public String getUserId() {
        return dataManager.getUserId();
    }

    @Override
    public void startTracking() {
        boolean isDriveInProgress = adtEventHandler.registerListener((LbtStateMachine) lbtStateMachine);
        if (isDriveInProgress) {
            Timber.v("Drive is currently in progress");
            ((LbtStateMachine) lbtStateMachine).onDrivingStart();
        }
        dataManager.setLbtAutoRestart(true);
    }

    @Override
    public void stopTracking() {
        adtEventHandler.unregisterListener((LbtStateMachine) lbtStateMachine);
        ((LbtStateMachine) lbtStateMachine).onDrivingStop();
        dataManager.setLbtAutoRestart(false);
    }

    @Override
    public void addTrackingListener(NccSensorListener sensorListener) throws NccException {
        ((LbtStateMachine) lbtStateMachine).addTrackingListener(sensorListener);
    }

    @Override
    public void removeTrackingListener(NccSensorListener sensorListener) throws NccException {
        ((LbtStateMachine) lbtStateMachine).removeTrackingListener(sensorListener);
    }

    @Override
    public void addDrivingListener(AdtEventListener adtEventListener) {
        adtEventHandler.registerListener(adtEventListener);
    }

    @Override
    public void removeDrivingListener(AdtEventListener adtEventListener) {
        adtEventHandler.unregisterListener(adtEventListener);
    }

    @Override
    public void setMonitoringNotification(@DrawableRes int drawableId, String contentTitle, String contentText) throws NccException {
        notificationManager.setNotification(ForegroundService.NOTIFICATION_ID, drawableId, contentTitle, contentText);
    }

    @Override
    public void setTrackingContext(TrackingContext trackingContext) {
        dataManager.setTrackingContext(trackingContext);
    }

    @Override
    public TrackingContext getTrackingContext() {
        return dataManager.getTrackingContext();
    }

    @Override
    public void clearTrackingContext() {
        dataManager.clearTrackingContext();
    }

    @Override
    public void setIgnoringBatteryOptimizationsCallback(IgnoringBatteryOptimizationsCallback ignoringBatteryOptimizationsCallback) {
        this.ignoringBatteryOptimizationsCallback = ignoringBatteryOptimizationsCallback;
    }

    @Override
    public void clearIgnoringBatteryOptimizationsCallback() {
        this.ignoringBatteryOptimizationsCallback = null;
    }
}