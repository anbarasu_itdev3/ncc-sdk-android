package com.agero.nccsdk.internal.data.preferences;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.internal.common.statemachine.SdkConfig;
import com.agero.nccsdk.internal.common.statemachine.SdkConfigType;

/**
 * Created by james hermida on 9/8/17.
 */

public interface ConfigManager {

    void saveConfig(NccSensorType sensorType, NccConfig config);
    NccConfig getConfig(NccSensorType sensorType) throws NccException;

    void saveSdkConfig(SdkConfigType configType, SdkConfig config);
    SdkConfig getSdkConfig(SdkConfigType configType) throws NccException;

}
