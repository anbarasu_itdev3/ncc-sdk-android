package com.agero.nccsdk.internal.common.util;

/**
 * Created by james hermida on 8/28/17.
 */

public class StringUtils {

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.length() == 0;
    }

    public static String capitalize(String s) {
        if (isNullOrEmpty(s)) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

}
