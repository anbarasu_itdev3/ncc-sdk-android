package com.agero.nccsdk.internal.data.network.rest.apigee;

import com.agero.nccsdk.internal.data.network.rest.apigee.model.ApigeeResponse;
import com.agero.nccsdk.internal.data.network.rest.apigee.model.AuthUserRequest;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApigeeService {

    @POST("v1/authenticateuser")
    Single<ApigeeResponse> authenticate(@Header("ApiKey") String apiKey, @Body AuthUserRequest body);

}
