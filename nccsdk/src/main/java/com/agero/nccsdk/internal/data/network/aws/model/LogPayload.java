package com.agero.nccsdk.internal.data.network.aws.model;

/**
 * Created by james hermida on 11/13/17.
 */

public class LogPayload extends BasePayload {

    private int level;
    private String message;
    private String stacktrace;
    private long timeUtc;
    private String timeLocal;

    public LogPayload(int level, String userId, String message) {
        super(userId);
        this.level = level;
        this.message = message;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStacktrace() {
        return stacktrace;
    }

    public void setStacktrace(String stacktrace) {
        this.stacktrace = stacktrace;
    }

    public long getTimeUtc() {
        return timeUtc;
    }

    public void setTimeUtc(long timeUtc) {
        this.timeUtc = timeUtc;
    }

    public String getTimeLocal() {
        return timeLocal;
    }

    public void setTimeLocal(String timeLocal) {
        this.timeLocal = timeLocal;
    }
}
