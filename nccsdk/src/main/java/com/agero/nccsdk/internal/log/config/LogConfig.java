package com.agero.nccsdk.internal.log.config;

import com.agero.nccsdk.BuildConfig;
import com.agero.nccsdk.internal.common.statemachine.SdkConfig;

/**
 * Created by james hermida on 11/13/17.
 */

public class LogConfig implements SdkConfig {

    private long uploadInterval = 1000 * 60 * 30; // default 30 minutes

    private int minLogLevel = BuildConfig.MIN_LOG_LEVEL;

    public LogConfig() {
    }

    public LogConfig(long uploadInterval) {
        this.uploadInterval = uploadInterval;
    }

    public long getUploadInterval() {
        return uploadInterval;
    }

    public void setUploadInterval(long uploadInterval) {
        this.uploadInterval = uploadInterval;
    }

    public int getMinLogLevel() {
        return minLogLevel;
    }

    public void setMinLogLevel(int minLogLevel) {
        this.minLogLevel = minLogLevel;
    }
}
