package com.agero.nccsdk.internal.di.module;

import android.content.Context;

import com.agero.nccsdk.BuildConfig;
import com.agero.nccsdk.internal.auth.AuthManager;
import com.agero.nccsdk.internal.auth.NccAuthManager;
import com.agero.nccsdk.internal.data.DataManager;
import com.agero.nccsdk.internal.data.NccDataManager;
import com.agero.nccsdk.internal.data.memory.ClientCache;
import com.agero.nccsdk.internal.data.memory.NccClientCache;
import com.agero.nccsdk.internal.data.network.aws.iot.IotClient;
import com.agero.nccsdk.internal.data.network.aws.s3.FileTransferManager;
import com.agero.nccsdk.internal.data.network.aws.s3.S3TransferManager;
import com.agero.nccsdk.internal.data.network.rest.apigee.ApigeeService;
import com.agero.nccsdk.internal.data.preferences.ConfigManager;
import com.agero.nccsdk.internal.data.preferences.NccConfigManager;
import com.agero.nccsdk.internal.data.preferences.NccSharedPrefs;
import com.agero.nccsdk.internal.data.preferences.SharedPrefs;
import com.agero.nccsdk.internal.di.qualifier.SdkLocalFilesPath;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by james hermida on 10/6/17.
 */

@Module
public class DataModule {

    private final String SP_FILE_NAME = "com.agero.nccsdk";

    @Provides
    @Singleton
    AuthManager provideAuthManager(Context context, DataManager dataManager, ApigeeService apigeeService) {
        return new NccAuthManager(context, dataManager, apigeeService);
    }

    @Provides
    @Singleton
    ApigeeService provideApigeeServices() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.APIGEE_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit.create(ApigeeService.class);
    }

    @Provides
    @Singleton
    AWSCredentialsProvider provideCognitoCachingCredentialsProvider(Context context, DataManager dataManager) {
        return new CognitoCachingCredentialsProvider(
                context,
                dataManager.getAccessId(),
                Regions.US_EAST_1
        );
    }

    @Provides // FIXME remove once NccSharedPrefs is refactored to only using high level APIs. See NccSharedPrefs FIXME.
    @Singleton
    NccSharedPrefs provideNccSharedPrefs(Context context) {
        return new NccSharedPrefs(context.getSharedPreferences(SP_FILE_NAME, Context.MODE_PRIVATE));
    }

    @Provides
    @Singleton
    SharedPrefs provideSharedPrefs(Context context) {
        return new NccSharedPrefs(context.getSharedPreferences(SP_FILE_NAME, Context.MODE_PRIVATE));
    }

    @Provides
    @Singleton
    IotClient provideIotClient(Context context, @SdkLocalFilesPath String sdkLocalFilesPath) {
        return new IotClient(context, sdkLocalFilesPath);
    }

    @Provides
    @Singleton
    FileTransferManager provideFileTransferManager(S3TransferManager s3TransferManager) {
        return s3TransferManager;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(NccDataManager nccDataManager) {
        return nccDataManager;
    }

    @Provides
    @Singleton
    ConfigManager provideConfigManager(NccConfigManager nccConfigManager) {
        return nccConfigManager;
    }

    @Provides
    @Singleton
    ClientCache provideClientCache() {
        return new NccClientCache();
    }

    @Provides
    @Singleton
    AmazonS3 provideAmazonS3(AWSCredentialsProvider cognitoCachingCredentialsProvider) {
        return new AmazonS3Client(cognitoCachingCredentialsProvider);
    }

    @Provides
    @Singleton
    TransferUtility provideTransferUtility(Context context, AmazonS3 amazonS3) {
        return new TransferUtility(amazonS3, context);
    }
}
