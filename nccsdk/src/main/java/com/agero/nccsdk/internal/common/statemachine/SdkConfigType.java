package com.agero.nccsdk.internal.common.statemachine;

/**
 * Created by james hermida on 11/8/17.
 */

public enum SdkConfigType {

    KINESIS("Kinesis"),

    SESSION("Session"),

    LOG("Log");

    private final String name;

    SdkConfigType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
