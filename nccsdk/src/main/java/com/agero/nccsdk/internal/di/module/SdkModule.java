package com.agero.nccsdk.internal.di.module;

import android.content.Context;
import android.os.AsyncTask;

import com.agero.nccsdk.internal.auth.AuthManager;
import com.agero.nccsdk.internal.common.util.Clock;
import com.agero.nccsdk.internal.common.util.NccClock;
import com.agero.nccsdk.internal.data.network.aws.kinesis.KinesisClient;
import com.agero.nccsdk.internal.data.network.aws.kinesis.LogKinesisClient;
import com.agero.nccsdk.internal.di.qualifier.LogDataPath;
import com.agero.nccsdk.internal.di.qualifier.SdkLocalFilesParentDirectory;
import com.agero.nccsdk.internal.di.qualifier.SdkLocalFilesPath;
import com.amazonaws.auth.AWSCredentialsProvider;

import java.io.File;
import java.util.concurrent.Executor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by james hermida on 9/21/17.
 */

@Module
public class SdkModule {
    /**
     * Parent directory for all data produced by the SDK
     */
    private final String SDK_LOCAL_FILES_PARENT_DIR = "data";

    private final String KINESIS_LOG_DATA_DIRECTORY = "log_data";

    private final Context applicationContext;

    private static final Object LOCK = new Object();
    private static volatile Executor sExecutor;

    public SdkModule(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return applicationContext;
    }

    @Provides
    @Singleton
    Clock provideClock() {
        return new NccClock();
    }

    @Provides
    Executor provideThreadPoolExecutor() {
        synchronized (LOCK) {
            if (sExecutor == null) {
                sExecutor = AsyncTask.THREAD_POOL_EXECUTOR;
            }
        }
        return sExecutor;
    }

    @Provides
    @SdkLocalFilesParentDirectory
    String provideSdkLocalFilesParentDirectory() {
        return SDK_LOCAL_FILES_PARENT_DIR;
    }

    @Provides
    @Singleton
    @LogDataPath
    File provideKinesisLogDataDirectory(@SdkLocalFilesPath String sdkLocalFilesPath) {
        return new File(sdkLocalFilesPath + File.separator + KINESIS_LOG_DATA_DIRECTORY);
    }

    @Provides
    @Singleton
    @com.agero.nccsdk.internal.di.qualifier.LogKinesisClient
    KinesisClient provideLogsStreamsKinesisClient(@LogDataPath File directory, AWSCredentialsProvider cognitoCachingCredentialsProvider, AuthManager authManager) {
        return new LogKinesisClient(directory, cognitoCachingCredentialsProvider, authManager);
    }
}
