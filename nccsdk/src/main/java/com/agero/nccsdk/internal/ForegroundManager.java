package com.agero.nccsdk.internal;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Usage:
 *
 * 1. Get the Foreground Singleton, passing a Context or Application object unless you
 * are sure that the Singleton has definitely already been initialised elsewhere.
 *
 * 2.a) Perform a direct, synchronous check: Foreground.isForeground() / .isBackground()
 *
 * or
 *
 * 2.b) Register to be notified (useful in Service or other non-UI components):
 *
 *   Foreground.Listener myListener = new Foreground.Listener(){
 *       public void onAppForeground(){
 *           // ... whatever you want to do
 *       }
 *       public void onAppBackground(){
 *           // ... whatever you want to do
 *       }
 *   }
 *
 *   public void onCreate(){
 *      super.onCreate();
 *      Foreground.get(this).addListener(listener);
 *   }
 *
 *   public void onDestroy(){
 *      super.onCreate();
 *      Foreground.get(this).removeListener(listener);
 *   }
 *
 */

public class ForegroundManager implements Application.ActivityLifecycleCallbacks {

    private final long CHECK_DELAY = 500;
    private final String TAG = ForegroundManager.class.getName();

    public interface Listener {
        void onAppForeground();
        void onAppBackground();
    }

    private static ForegroundManager instance;

    private boolean foreground = false, paused = true;
    private Handler handler = new Handler();
    private List<Listener> listeners = new CopyOnWriteArrayList<>();
    private Runnable check;

    /**
     * Its not strictly necessary to use this method - usually invoking
     * get with a Context gives us a path to retrieve the Application and
     * initialise, but sometimes (e.g. in test harness) the ApplicationContext
     * is != the Application, and the docs make no guarantees.
     *
     * @param application
     * @return an initialised Foreground instance
     */
    public static ForegroundManager init(Application application){
        if (instance == null) {
            instance = new ForegroundManager();
            application.registerActivityLifecycleCallbacks(instance);
        }
        return instance;
    }

    public static ForegroundManager get(Application application){
        if (instance == null) {
            init(application);
        }
        return instance;
    }

    public static ForegroundManager get(Context ctx){
        if (instance == null) {
            Context appCtx = ctx.getApplicationContext();
            if (appCtx instanceof Application) {
                init((Application)appCtx);
            }
            throw new IllegalStateException(
                    "Foreground is not initialised and " +
                            "cannot obtain the Application object");
        }
        return instance;
    }

    public static ForegroundManager get(){
        if (instance == null) {
            throw new IllegalStateException(
                    "Foreground is not initialised - invoke " +
                            "at least once with parameterised init/get");
        }
        return instance;
    }

    public boolean isForeground(){
        return foreground;
    }

    public boolean isBackground(){
        return !foreground;
    }

    public void addListener(Listener listener){
        listeners.add(listener);
    }

    public void removeListener(Listener listener){
        listeners.remove(listener);
    }

    @Override
    public void onActivityResumed(Activity activity) {
        paused = false;
        boolean wasBackground = !foreground;
        foreground = true;

        // remove the check callback if there is one, to cancel the pending notification of going background
        if (check != null)
            handler.removeCallbacks(check);

        if (wasBackground){
            if (listeners != null && listeners.size() > 0) {
                // Send listeners backwards since the last listener in the list will always be the activity on top of the backstack
                // Note: This reverse order is required for requesting permissions on app background to function correctly for
                // the activity on top of the backstack
                for (int i = listeners.size() - 1; i >= 0; i--) {
                    try {
                        listeners.get(i).onAppForeground();
                    } catch (Exception exc) {
                        Log.e(TAG, "Listener threw exception!", exc);
                    }
                }
            }
        } else {
            Log.i(TAG, "App still in foreground");
        }
    }

    @Override
    public void onActivityPaused(Activity activity) {
        paused = true;

        if (check != null)
            handler.removeCallbacks(check);

        // schedule a Runnable to execute after CHECKDELAY milliseconds,
        // and capture the Runnable in the check member variable so it can be cancelled if necessary
        handler.postDelayed(check = () -> {
            if (foreground && paused) {
                foreground = false;
                Log.d(TAG, "App now in background");
                for (Listener l : listeners) {
                    try {
                        l.onAppBackground();
                    } catch (Exception exc) {
                        Log.e(TAG, "Listener threw exception!", exc);
                    }
                }
            } else {
                Log.i(TAG, "App still in foreground");
            }
        }, CHECK_DELAY);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {}

    @Override
    public void onActivityStarted(Activity activity) {}

    @Override
    public void onActivityStopped(Activity activity) {}

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {}

    @Override
    public void onActivityDestroyed(Activity activity) {}

}
