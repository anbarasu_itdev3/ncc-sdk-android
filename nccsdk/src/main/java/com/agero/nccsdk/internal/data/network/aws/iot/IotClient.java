package com.agero.nccsdk.internal.data.network.aws.iot;

import android.content.Context;

import com.agero.nccsdk.internal.common.util.FileUtils;
import com.agero.nccsdk.internal.di.qualifier.SdkLocalFilesPath;
import com.agero.nccsdk.internal.log.Timber;
import com.amazonaws.mobileconnectors.iot.AWSIotKeystoreHelper;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttClientStatusCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttManager;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttNewMessageCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttQos;

import java.io.File;
import java.security.InvalidParameterException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by james hermida on 9/15/17.
 */

@Singleton
public class IotClient {

    private Context applicationContext;

    private AWSIotMqttManager mqttManager;
    private final AWSIotMqttQos DEFAULT_MQTT_QOS = AWSIotMqttQos.QOS0;

    private List<OnConnectedCallback> onConnectedCallbacks;

    public interface OnConnectedCallback {
        void onConnected();
    }

    void addOnConnectedCallback(OnConnectedCallback callback) {
        if (callback == null) {
            throw new InvalidParameterException("listener cannot be null");
        } else {
            if (onConnectedCallbacks == null) {
                onConnectedCallbacks = new ArrayList<>();
            }
            onConnectedCallbacks.add(callback);
        }
    }

    @Inject
    public IotClient(Context context, @SdkLocalFilesPath String sdkLocalFilesPath) {
        if (context == null) {
            throw new IllegalArgumentException("Context cannot be null");
        }

        this.applicationContext = context;

//        // MQTT client IDs are required to be unique per AWS IoT account.
//        // This UUID is "practically unique" but does not _guarantee_
//        // uniqueness.
//        String mqttClientId = UUID.randomUUID().toString();
//
//        // MQTT Client
//        mqttManager = new AWSIotMqttManager(mqttClientId, BuildConfig.IOT_ENDPOINT);
//
//        // Set keepalive to 10 seconds.  Will recognize disconnects more quickly but will also send
//        // MQTT pings every 10 seconds.
//        mqttManager.setKeepAlive(10);
//
//        // Set Last Will and Testament for MQTT.  On an unclean disconnect (loss of connection)
//        // AWS IoT will publish this message to alert other clients.
//        AWSIotMqttLastWillAndTestament lwt = new AWSIotMqttLastWillAndTestament("my/lwt/topic",
//                "Android client lost connection", DEFAULT_MQTT_QOS);
//        mqttManager.setMqttLastWillAndTestament(lwt);
//
//        // Init keystore
//        KeyStore keyStore = loadCertKey(
//                sdkLocalFilesPath,
//                BuildConfig.IOT_CERT_FILE_NAME,
//                BuildConfig.IOT_CERT_ALIAS,
//                BuildConfig.IOT_CERT_PW
//        );
//
//        // Check keystore and connect
//        if (keyStore == null) {
//            Timber.e("Iot cert/key was not found in keystore");
//        } else {
//            connect(keyStore);
//        }
    }
    
    private KeyStore loadCertKey(String certPath, String certFileName, String certAlias, String certPw) {
        KeyStore keyStore = null;

        try {
            // Load keystore from assets directory
            File keystoreFile = new File(certPath + File.separator + certFileName);
            FileUtils.writeAssetToFile(applicationContext, certFileName, keystoreFile);

            if (AWSIotKeystoreHelper.isKeystorePresent(certPath, certFileName)) {
                if (AWSIotKeystoreHelper.keystoreContainsAlias(certAlias, certPath,
                        certFileName, certPw)) {
                    Timber.v("Iot certificate found in keystore - using for MQTT.");
                    // load keystore from file into memory to pass on connection
                    keyStore = AWSIotKeystoreHelper.getIotKeystore(certAlias,
                            certPath, certFileName, certPw);
                } else {
                    Timber.e("Iot key/cert not found in keystore.");
                }
            } else {
                Timber.e("Iot keystore not found.");
            }
        } catch (Exception e) {
            Timber.e(e, "An error occurred retrieving Iot cert/key from keystore.");
        }

        return keyStore;
    }
    
    private void connect(KeyStore keyStore) {
        try {
            mqttManager.connect(keyStore, (status, throwable) -> {
                if (status == AWSIotMqttClientStatusCallback.AWSIotMqttClientStatus.Connecting) {
                    Timber.v("MqttManager connecting...");
                } else if (status == AWSIotMqttClientStatusCallback.AWSIotMqttClientStatus.Connected) {
                    Timber.v("MqttManager connected");

                    for (OnConnectedCallback listener : onConnectedCallbacks) {
                        listener.onConnected();
                    }
                } else if (status == AWSIotMqttClientStatusCallback.AWSIotMqttClientStatus.Reconnecting) {
                    if (throwable != null) {
                        Timber.e(throwable, "MqttManager connection error.");
                    }
                    Timber.v("MqttManager reconnecting");
                } else if (status == AWSIotMqttClientStatusCallback.AWSIotMqttClientStatus.ConnectionLost) {
                    if (throwable != null) {
                        Timber.e(throwable, "MqttManager connection error.");
                    }
                    Timber.v("MqttManager disconnected");
                } else {
                    Timber.v("MqttManager disconnected");
                }
            });
        } catch (final Exception e) {
            Timber.e(e, "MqttManager connection error.");
        }
    }

    private void disconnect() {
        try {
            mqttManager.disconnect();
        } catch (Exception e) {
            Timber.e(e, "MqttManager disconnect error.");
        }
    }

    public void subscribeToTopic(String topic, AWSIotMqttNewMessageCallback callback) {
        // TODO check if mqttManager is connected, if not, subscribe call should be executed when connected
        try {
            mqttManager.subscribeToTopic(topic, DEFAULT_MQTT_QOS, callback);
            Timber.d("Subscribed to topic: %s", topic);
        } catch (Exception e) {
            Timber.e(e, "Error subscribing to topic: ", topic);
        }
    }

    void publish(String msg) {
        try {
            mqttManager.publishString(msg, "my_topic", DEFAULT_MQTT_QOS);
        } catch (Exception e) {
            Timber.e(e, "Error publishing msg: %s", msg);
        }
    }
}
