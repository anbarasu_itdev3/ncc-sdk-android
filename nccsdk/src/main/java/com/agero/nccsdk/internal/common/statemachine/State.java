package com.agero.nccsdk.internal.common.statemachine;

/**
 * Created by james hermida on 11/6/17.
 */

public interface State {

    void onEnter();
    void onExit();

}
