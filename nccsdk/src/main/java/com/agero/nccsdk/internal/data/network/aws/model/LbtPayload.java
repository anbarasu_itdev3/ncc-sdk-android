package com.agero.nccsdk.internal.data.network.aws.model;

/**
 * Created by james hermida on 10/11/17.
 */

public class LbtPayload extends BasePayload {

    private final String dataType;
    private final String data;

    public LbtPayload(String userId, String dataType, String data) {
        super(userId);
        this.dataType = dataType;
        this.data = data;
    }

    public String getDataType() {
        return dataType;
    }

    public String getData() {
        return data;
    }
}
