package com.agero.nccsdk.adt.detection.start.algo;

import android.content.Context;
import android.os.Handler;

import com.agero.nccsdk.adt.AdtTriggerType;
import com.agero.nccsdk.adt.detection.start.model.WifiActivity;
import com.agero.nccsdk.adt.detection.start.model.WifiInfo;
import com.agero.nccsdk.domain.data.NccWifiData;
import com.agero.nccsdk.internal.data.DataManager;
import com.agero.nccsdk.adt.config.SessionConfig;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;

import static android.net.ConnectivityManager.TYPE_WIFI;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * Created by james hermida on 12/1/17.
 */

@RunWith(PowerMockRunner.class)
public class WifiDrivingStartStrategyTest {

    @Mock
    private Context mockContext;

    @Mock
    private DataManager mockDataManager;

    @Mock
    private Handler mockHandler;

    private SessionConfig mockConfig;

    private WifiDrivingStartStrategy wifiCollectionStartStrategy;

    @Before
    public void setUp() throws Exception {
        mockConfig = new SessionConfig();
        mockConfig.setWifiReconnectTimeThreshold(1000);

        wifiCollectionStartStrategy = PowerMockito.spy(new WifiDrivingStartStrategy(mockContext, mockConfig, mockDataManager));

        mockPrivateField(wifiCollectionStartStrategy, "handler", mockHandler);
    }

    @Test
    public void evaluate_null_data() throws Exception {
        wifiCollectionStartStrategy.evaluate(null);

        verifyZeroInteractions(mockContext);
        verifyZeroInteractions(mockDataManager);
    }

    @Test
    public void evaluate_invalid_network_type() throws Exception {
        int type = -1;

        NccWifiData data = new NccWifiData(0, type, "", "", false, false);
        wifiCollectionStartStrategy.evaluate(data);

        verifyZeroInteractions(mockContext);
        verifyZeroInteractions(mockDataManager);
    }

    @Test
    public void evaluate_connect_no_recent_disconnect() throws Exception {
        WifiActivity testWifiActivity = new WifiActivity(new WifiInfo("", 0), null);
        PowerMockito.when(mockDataManager.getRecentWifiActivity()).thenReturn(testWifiActivity);

        mockRunnablePosted(true);

        long timestamp = 0;
        int type = TYPE_WIFI;
        boolean isConnected = true;
        NccWifiData data = new NccWifiData(timestamp, type, "", "", isConnected, false);

        wifiCollectionStartStrategy.evaluate(data);

        verify(mockHandler, times(0)).removeCallbacks(wifiCollectionStartStrategy);
    }

    @Test
    public void evaluate_connect_ssid_differs_from_recent_disconnect_ssid() throws Exception {
        String recentDisconnectedWifiSsid = "differentSsid";
        WifiActivity testWifiActivity = getTestWifiActivity("", 0, recentDisconnectedWifiSsid, 100);
        PowerMockito.when(mockDataManager.getRecentWifiActivity()).thenReturn(testWifiActivity);

        mockRunnablePosted(true);

        long timestamp = 200;
        int type = TYPE_WIFI;
        boolean isConnected = true;
        NccWifiData data = new NccWifiData(timestamp, type, "", "", isConnected, false);

        wifiCollectionStartStrategy.evaluate(data);

        verify(mockHandler, times(0)).removeCallbacks(wifiCollectionStartStrategy);
    }

    @Test
    public void evaluate_reconnect_elapsed_time_above_threshold() throws Exception {
        String recentDisconnectedWifiSsid = "wifiSsid";
        WifiActivity testWifiActivity = getTestWifiActivity("", 0, recentDisconnectedWifiSsid, mockConfig.getWifiReconnectTimeThreshold());
        PowerMockito.when(mockDataManager.getRecentWifiActivity()).thenReturn(testWifiActivity);

        mockRunnablePosted(true);

        long timestamp = mockConfig.getWifiReconnectTimeThreshold() * 2;
        int type = TYPE_WIFI;
        boolean isConnected = true;
        NccWifiData data = new NccWifiData(timestamp, type, "", "", isConnected, false);

        wifiCollectionStartStrategy.evaluate(data);

        verify(mockHandler, times(0)).removeCallbacks(wifiCollectionStartStrategy);
    }

    @Test
    public void evaluate_connect_runnable_not_posted() throws Exception {
        String recentDisconnectedWifiSsid = "wifiSsid";
        WifiActivity testWifiActivity = getTestWifiActivity("", 0, recentDisconnectedWifiSsid, 100);
        PowerMockito.when(mockDataManager.getRecentWifiActivity()).thenReturn(testWifiActivity);

        mockRunnablePosted(false);

        long timestamp = 200;
        int type = TYPE_WIFI;
        boolean isConnected = true;
        NccWifiData data = new NccWifiData(timestamp, type, "", "", isConnected, false);

        wifiCollectionStartStrategy.evaluate(data);

        verify(mockHandler, times(0)).removeCallbacks(wifiCollectionStartStrategy);
    }

    @Test
    public void evaluate_reconnect_removes_runnable() throws Exception {
        String ssid = "wifiSsid";
        WifiActivity testWifiActivity = getTestWifiActivity(ssid, 0, ssid, 100);
        PowerMockito.when(mockDataManager.getRecentWifiActivity()).thenReturn(testWifiActivity);

        mockRunnablePosted(true);

        long timestamp = 200;
        int type = TYPE_WIFI;
        boolean isConnected = true;
        NccWifiData data = new NccWifiData(timestamp, type, "", ssid, isConnected, false);

        wifiCollectionStartStrategy.evaluate(data);

        verify(mockHandler).removeCallbacks(wifiCollectionStartStrategy);
    }

    @Test
    public void evaluate_connecting() throws Exception {
        boolean isConnectedOrConnecting = true;

        NccWifiData data = new NccWifiData(0, 0, "", "", false, isConnectedOrConnecting);
        wifiCollectionStartStrategy.evaluate(data);

        verifyZeroInteractions(mockContext);
        verifyZeroInteractions(mockDataManager);
    }

    @Test
    public void evaluate_disconnect_runnable_not_posted() throws Exception {
        WifiActivity testWifiActivity = getTestWifiActivity("test", 0, "", 0);
        PowerMockito.when(mockDataManager.getRecentWifiActivity()).thenReturn(testWifiActivity);

        mockRunnablePosted(true);

        int type = TYPE_WIFI;
        boolean isConnected = false;
        boolean isConnectedOrConnecting = false;
        NccWifiData data = new NccWifiData(0, type, "", "", isConnected, isConnectedOrConnecting);

        wifiCollectionStartStrategy.evaluate(data);

        verify(mockHandler, times(0)).postDelayed(wifiCollectionStartStrategy, mockConfig.getWifiDisconnectStartSessionDelay());
    }

    @Test
    public void evaluate_disconnect_starts_service() throws Exception {
        WifiActivity testWifiActivity = getTestWifiActivity("test", 0, "", 0);
        PowerMockito.when(mockDataManager.getRecentWifiActivity()).thenReturn(testWifiActivity);

        mockRunnablePosted(false);

        int type = TYPE_WIFI;
        boolean isConnected = false;
        boolean isConnectedOrConnecting = false;
        NccWifiData data = new NccWifiData(0, type, "", "", isConnected, isConnectedOrConnecting);

        wifiCollectionStartStrategy.evaluate(data);

        verify(mockHandler).postDelayed(wifiCollectionStartStrategy, mockConfig.getWifiDisconnectStartSessionDelay());
    }

    @Test
    public void run() throws Exception {
        PowerMockito.doNothing().when(wifiCollectionStartStrategy, "sendCollectionStartBroadcast", any(AdtTriggerType.class));

        wifiCollectionStartStrategy.run();
        PowerMockito.verifyPrivate(wifiCollectionStartStrategy).invoke("sendCollectionStartBroadcast", AdtTriggerType.START_WIFI);
    }

    private WifiActivity getTestWifiActivity(String connectedSsid, long connectedTimestamp, String disconnectedSsid, long disconnectedTimestamp) {
        return new WifiActivity(
                new WifiInfo(connectedSsid, connectedTimestamp),
                new WifiInfo(disconnectedSsid, disconnectedTimestamp)
        );
    }

    private void mockRunnablePosted(boolean posted) throws Exception {
        Field field = WifiDrivingStartStrategy.class.getDeclaredField("isStartSessionRunnablePosted");
        field.setAccessible(true);
        field.set(wifiCollectionStartStrategy, posted);
    }

    private void mockPrivateField(Object targetObject, String fieldName, Object fieldValue) throws Exception {
        Field field = WifiDrivingStartStrategy.class.getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(targetObject, fieldValue);
    }
}
