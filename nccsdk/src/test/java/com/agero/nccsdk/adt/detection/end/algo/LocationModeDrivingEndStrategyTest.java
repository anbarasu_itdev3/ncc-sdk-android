package com.agero.nccsdk.adt.detection.end.algo;

import android.content.Context;

import com.agero.nccsdk.domain.data.NccLocationSettingsData;
import com.agero.nccsdk.adt.AdtTriggerType;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by james hermida on 11/29/17.
 */

@RunWith(PowerMockRunner.class)
public class LocationModeDrivingEndStrategyTest {

    @Mock
    private Context mockContext;

    private LocationModeDrivingEndStrategy locationModeCollectionEndStrategy;

    @Before
    public void setUp() throws Exception {
        locationModeCollectionEndStrategy = PowerMockito.spy(new LocationModeDrivingEndStrategy(mockContext));
    }

    @Test
    @PrepareForTest({LocationModeDrivingEndStrategy.class})
    public void evaluate_null_data() throws Exception {
        locationModeCollectionEndStrategy.evaluate(null);

        verify(locationModeCollectionEndStrategy, times(0)).sendCollectionEndBroadcast(any(AdtTriggerType.class));
    }

    @Test
    @PrepareForTest({LocationModeDrivingEndStrategy.class})
    public void evaluate_continue_collection_valid_mode() throws Exception {
        PowerMockito.when(locationModeCollectionEndStrategy, "isKitKatOrAbove").thenReturn(true);

        NccLocationSettingsData data = new NccLocationSettingsData(0, NccLocationSettingsData.Mode.HIGH_ACCURACY);
        locationModeCollectionEndStrategy.evaluate(data);

        verify(locationModeCollectionEndStrategy, never()).sendCollectionEndBroadcast(AdtTriggerType.STOP_LOCATION_MODE);
    }

    @Test
    @PrepareForTest({LocationModeDrivingEndStrategy.class})
    public void evaluate_ends_collection_mode_unavailable() throws Exception {
        PowerMockito.doNothing().when(locationModeCollectionEndStrategy, "sendCollectionEndBroadcast", any(AdtTriggerType.class));
        PowerMockito.when(locationModeCollectionEndStrategy, "isKitKatOrAbove").thenReturn(true);

        NccLocationSettingsData data = new NccLocationSettingsData(0, NccLocationSettingsData.Mode.UNAVAILABLE);
        locationModeCollectionEndStrategy.evaluate(data);

        verify(locationModeCollectionEndStrategy, times(1)).sendCollectionEndBroadcast(AdtTriggerType.STOP_LOCATION_MODE);
    }

    @Test
    @PrepareForTest({LocationModeDrivingEndStrategy.class})
    public void evaluate_ends_collection_mode_off() throws Exception {
        PowerMockito.doNothing().when(locationModeCollectionEndStrategy, "sendCollectionEndBroadcast", any(AdtTriggerType.class));
        PowerMockito.when(locationModeCollectionEndStrategy, "isKitKatOrAbove").thenReturn(true);

        NccLocationSettingsData data = new NccLocationSettingsData(0, NccLocationSettingsData.Mode.OFF);
        locationModeCollectionEndStrategy.evaluate(data);

        verify(locationModeCollectionEndStrategy, times(1)).sendCollectionEndBroadcast(AdtTriggerType.STOP_LOCATION_MODE);
    }

    @Test
    @PrepareForTest({LocationModeDrivingEndStrategy.class})
    public void evaluate_ends_collection_mode_sensors_only() throws Exception {
        PowerMockito.doNothing().when(locationModeCollectionEndStrategy, "sendCollectionEndBroadcast", any(AdtTriggerType.class));
        PowerMockito.when(locationModeCollectionEndStrategy, "isKitKatOrAbove").thenReturn(true);

        NccLocationSettingsData data = new NccLocationSettingsData(0, NccLocationSettingsData.Mode.SENSORS_ONLY);
        locationModeCollectionEndStrategy.evaluate(data);

        verify(locationModeCollectionEndStrategy, times(1)).sendCollectionEndBroadcast(AdtTriggerType.STOP_LOCATION_MODE);
    }

    @Test
    @PrepareForTest({LocationModeDrivingEndStrategy.class})
    public void evaluate_ends_collection_mode_battery_saving() throws Exception {
        PowerMockito.doNothing().when(locationModeCollectionEndStrategy, "sendCollectionEndBroadcast", any(AdtTriggerType.class));
        PowerMockito.when(locationModeCollectionEndStrategy, "isKitKatOrAbove").thenReturn(true);

        NccLocationSettingsData data = new NccLocationSettingsData(0, NccLocationSettingsData.Mode.BATTERY_SAVING);
        locationModeCollectionEndStrategy.evaluate(data);

        verify(locationModeCollectionEndStrategy, times(1)).sendCollectionEndBroadcast(AdtTriggerType.STOP_LOCATION_MODE);
    }

    @Test
    @PrepareForTest({LocationModeDrivingEndStrategy.class})
    public void evaluate_continue_collection_valid_providers() throws Exception {
        PowerMockito.when(locationModeCollectionEndStrategy, "isKitKatOrAbove").thenReturn(false);

        NccLocationSettingsData data = new NccLocationSettingsData(0, new NccLocationSettingsData.Provider(true, true));
        locationModeCollectionEndStrategy.evaluate(data);

        verify(locationModeCollectionEndStrategy, never()).sendCollectionEndBroadcast(AdtTriggerType.STOP_LOCATION_MODE);
    }

    @Test
    @PrepareForTest({LocationModeDrivingEndStrategy.class})
    public void evaluate_ends_collection_provider_disabled_gps() throws Exception {
        PowerMockito.doNothing().when(locationModeCollectionEndStrategy, "sendCollectionEndBroadcast", any(AdtTriggerType.class));
        PowerMockito.when(locationModeCollectionEndStrategy, "isKitKatOrAbove").thenReturn(false);

        NccLocationSettingsData data = new NccLocationSettingsData(0, new NccLocationSettingsData.Provider(false, true));
        locationModeCollectionEndStrategy.evaluate(data);

        verify(locationModeCollectionEndStrategy, times(1)).sendCollectionEndBroadcast(AdtTriggerType.STOP_LOCATION_MODE);
    }

    @Test
    @PrepareForTest({LocationModeDrivingEndStrategy.class})
    public void evaluate_ends_collection_provider_disabled_network() throws Exception {
        PowerMockito.doNothing().when(locationModeCollectionEndStrategy, "sendCollectionEndBroadcast", any(AdtTriggerType.class));
        PowerMockito.when(locationModeCollectionEndStrategy, "isKitKatOrAbove").thenReturn(false);

        NccLocationSettingsData data = new NccLocationSettingsData(0, new NccLocationSettingsData.Provider(true, false));
        locationModeCollectionEndStrategy.evaluate(data);

        verify(locationModeCollectionEndStrategy, times(1)).sendCollectionEndBroadcast(AdtTriggerType.STOP_LOCATION_MODE);
    }
}
