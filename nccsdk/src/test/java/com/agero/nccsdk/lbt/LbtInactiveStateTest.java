package com.agero.nccsdk.lbt;

import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.internal.common.statemachine.StateMachine;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.lang.reflect.Field;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * Created by james hermida on 11/16/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class LbtInactiveStateTest {

    private LbtInactiveState lbtInactiveState;

    @Mock
    private StateMachine<LbtState> mockStateMachine;

    @Before
    public void setUp() throws Exception {
        lbtInactiveState = new LbtInactiveState();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void addTrackingListener() throws Exception {
        NccSensorListener sensorListener = (sensorType, data) -> {};
        lbtInactiveState.addTrackingListener(mockStateMachine, sensorListener);
        List<NccSensorListener> listeners = (List<NccSensorListener>) getListeners(lbtInactiveState);
        assertTrue(listeners.size() == 1);
        assertTrue(listeners.contains(sensorListener));

        // Assert same listener is not adding to list again
        lbtInactiveState.addTrackingListener(mockStateMachine, sensorListener);
        listeners = (List<NccSensorListener>) getListeners(lbtInactiveState);
        assertTrue(listeners.size() == 1);
        assertTrue(listeners.contains(sensorListener));

        // Assert additional listener is added
        NccSensorListener newSensorListener = (sensorType, data) -> {};
        lbtInactiveState.addTrackingListener(mockStateMachine, newSensorListener);
        listeners = (List<NccSensorListener>) getListeners(lbtInactiveState);
        assertTrue(listeners.size() == 2);
        assertTrue(listeners.contains(newSensorListener));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void removeTrackingListener() throws Exception {
        NccSensorListener sensorListener = (sensorType, data) -> {};
        lbtInactiveState.addTrackingListener(mockStateMachine, sensorListener);
        lbtInactiveState.removeTrackingListener(mockStateMachine, sensorListener);
        List<NccSensorListener> listeners = (List<NccSensorListener>) getListeners(lbtInactiveState);
        assertFalse(listeners.contains(sensorListener));
        assertTrue(listeners.size() == 0);
    }

    @Test
    public void onDrivingStart() throws Exception {
        lbtInactiveState.onDrivingStart(mockStateMachine);
        verify(mockStateMachine).setState(any(LbtCollectionState.class));
    }

    @Test
    public void onDrivingStop() throws Exception {
        lbtInactiveState.onDrivingStop(mockStateMachine);
        verifyZeroInteractions(mockStateMachine);
    }

    private Object getListeners(LbtInactiveState inactiveState) {
        try {
            Field f = getField(LbtInactiveState.class, "trackingListeners");
            f.setAccessible(true);
            f.get(inactiveState);

            return f.get(inactiveState);
        } catch (NoSuchFieldException nsfe) {
            nsfe.printStackTrace();
        } catch (IllegalAccessException iae) {
            iae.printStackTrace();
        }

        return null;
    }

    private Field getField(Class clazz, String fieldName) throws NoSuchFieldException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            Class superClass = clazz.getSuperclass();
            if (superClass == null) {
                throw e;
            } else {
                return getField(superClass, fieldName);
            }
        }
    }
}
